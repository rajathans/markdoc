<?php

namespace App\Http\Controllers;

use Auth;
use Curl;
use App\User;
use App\Http\Controllers\Controller;

use Socialite;

/**
 * Social auth controller
 */
class AuthController extends Controller
{
	/**
     * Redirect the user to github
     * 
     * @return [type] [description]
     */
    public function redirectToProvider()
    {
        return Socialite::driver('github')->scopes(['user', 'repo'])->redirect();
    }

    /**
     * Get user info from github and login user
     * 
     * @return [type] [description]
     */
    public function handleProviderCallback()
    {
        try {   
            $user = Socialite::driver('github')->user();
        } catch (Exception $e) {
            return redirect()->route('/auth/login');
        }

        $authUser = User::where('github_id', $user->id)->first();

        if (empty($authUser))
        {
            $authUser = User::create([
                'name' => is_null($user->name) ? $user->nickname : $user->name,
                'username' => $user->nickname,
                'email' => empty($user->email) ? '' : $user->email, 
                'github_id' => $user->id,
                'avatar' => $user->avatar,
            ]);
        }

        // // If user data fetched from Github using Socialite does not have email
        // if (empty($authUser->email)) 
        // {
        //     $user_data = Curl::to('https://api.github.com/user')
        //         ->withHeader('Authorization: token '. $user->token)
        //         ->withOption('USERAGENT', 'MarkDoc')
        //         ->asJson()
        //         ->get();

        //     $authUser->email = $user_data->email;
        // }

        $authUser->token = $user->token;
        $authUser->save();

        Auth::login($authUser, true);

        return redirect()->route('home');
    }
}
