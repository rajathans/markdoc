<?php

namespace App\Http\Controllers;

use Auth;
use Curl;
use View;
use DB;
use DirectoryIterator;

use App\Repository;
use App\RepoContent;

use App\Http\Requests;
use Illuminate\Http\Request;

class RepositoryController extends Controller
{
	public function __construct()
	{
	    $this->middleware('auth');
	}

    /**
     * Show the application home
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {	
    	$user = Auth::user();

    	$repositories = Curl::to('https://api.github.com/user/repos')
		        ->withHeader('Authorization: token '. $user->token)
		        ->withOption('USERAGENT', 'MarkDoc')
		        ->withOption('RETURNTRANSFER', true)
		        ->asJson()
		        ->get();

		$linked = Repository::select('repo_id')
					->where('user_id', $user->id)
					->get()
					->pluck('repo_id')
					->toArray();

		// return $repositories;

        return View::make('home')->with('repositories', $repositories)->with('linked', $linked);
    }

    /**
     * Select and download repository from list of repositories
     * 
     * @return [type] [description]
     */
    public function download(Request $request)
    {
    	$user = Auth::user();
    	
    	$zippedFileName = basename($request->url);
    	file_put_contents( $zippedFileName.'.zip', fopen(rtrim($request->url, ' ').'/archive/master.zip', 'r') );

    	// Unqip contents
    	$zip = new \ZipArchive;

		if ($zip->open($zippedFileName.'.zip') === TRUE) 
		{
		    $zip->extractTo('repos/'.$request->id);
		    $zip->close();

		    // Delete .zip file
			unlink($zippedFileName.'.zip');

            // TODO : Add repository contents to mongo
            // $this->addRepoDataToMongo($data);

			Repository::create([
					'repo_id' => $request->id,
					'user_id' => $user->id,
					'url' => $request->url,
					'branch' => 'master',
                    'status' => '1'
				]);
		}

    	return back();
    }

    /**
     * Add repository data to mongo ans show its contents in json
     * 
     * @param [type] $data [description]
     */
    public function store($repo_id)
    {  
        $structure = getFileData(new DirectoryIterator(public_path().'/repos/'.$repo_id));
        RepoContent::create( [ 'repo_id' => $repo_id, 'data' => $structure] );
        $data = RepoContent::where('repo_id', $repo_id)->get();
        // rrmdir(public_path().'/repos/'.$repo_id);
        return $data;
        // return array_keys($data->toArray());
    }
}
