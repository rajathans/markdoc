<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

// Social auth routes
Route::group(['prefix' => 'auth'], function () {

	// Redirect to github
	Route::get('github', 'AuthController@redirectToProvider');

	// Callback
	Route::get('github/callback', 'AuthController@handleProviderCallback');
});

Route::get('home', [
  	'as' => 'home',
    'uses' => 'RepositoryController@index'
]);

// Repo related routes
Route::group(['prefix' => 'repo'], function () {

	Route::post('download', 'RepositoryController@download');
	Route::get('store/{id}', 'RepositoryController@store');
	
});
