<?php

if ( !function_exists('getFileData'))
{
    /**
     * Get folder structure in array form
     * 
     * @param type $string
     */
    function getFileData(DirectoryIterator $dir)
    {
        // return $dir->getPathname();
        $data = [];
        foreach ( $dir as $node )
        {
            if ( $node->isDir() && !$node->isDot() )
                $data[$node->getFilename()] = getFileData( new DirectoryIterator( $node->getPathname() ) );
            else if ( $node->isFile() )
            {
                $file_data = file_get_contents($dir->getPathname());
                $data[$node->getFilename()] = json_encode($file_data);
            }
        }
        
        return $data;
    }
}


if ( !function_exists('rrmdir') ) 
{
    function rrmdir($dir) 
    { 
        if (is_dir($dir)) 
        { 
            $objects = scandir($dir); 
            foreach ($objects as $object) 
            { 
                if ($object != "." && $object != "..") 
                { 
                    if (is_dir($dir."/".$object))
                        rrmdir($dir."/".$object);
                    else
                        unlink($dir."/".$object); 
                } 
            }
            rmdir($dir); 
        } 
    }
}
