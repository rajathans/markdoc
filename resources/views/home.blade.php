@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 well" style="background-color: white;">
           
                    @foreach ($repositories as $repository)

                        <!-- <b>{{ $repository->id }}</b><br> -->
                        <a style="font-weight: bold;" href="{{ $repository->clone_url }}" style="font-size: 20px;">{{ $repository->clone_url }}</a>
                        <p style="font-size: 15px;">{{ $repository->description }}</p>     

                        <form action="repo/download" method="POST">
                            {{ csrf_field() }}

                            <input type="hidden" name="url" value="{{ $repository->html_url }}">
                            <input type="hidden" name="id" value="{{ $repository->id }}">
                            @if (!in_array($repository->id, $linked))
                                <button type="submit" class="btn btn-danger">
                                    Sync
                                </button>
                            @else
                                <a href="repo/store/{{ $repository->id }}" class="btn btn-primary">
                                    Show content
                                </a>
                                <button type="submit" class="btn btn-info">
                                    Re-sync
                                </button>
                            @endif

                        </form>
                        <br>

                    @endforeach

        </div>
    </div>
</div>
@endsection
