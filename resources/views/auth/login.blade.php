@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <a href="/auth/github" class="btn btn-success">
                        <i class="fa fa-btn fa-sign-in"></i>Login with GitHub
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
