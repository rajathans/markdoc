@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <p align="center" style="font-size: 60px;">Welcome to MarkDoc!</p>
        </div>
    </div>
</div>
@endsection
